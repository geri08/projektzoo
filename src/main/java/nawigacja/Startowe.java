package nawigacja;

import zwierzeta.Animal;
import zwierzeta.ListaZwierzat;

import java.util.Scanner;

public class Startowe {
    private static Scanner scanner = new Scanner(System.in);

    public static void powitanie() {
        System.out.println("witaj w naszym zoo !!");
        System.out.println("u nas spotkasz rózne, egzotyczne zwięrzęta");
        System.out.println("możesz  się z nimi pobawić, nakarmić je");
        System.out.println("ale pamietaj, jak będą zmęczone daj im chwilkę odpocząć i nabrać sił");
        System.out.println("podaj z klawiatury odpowiednią cyfrę aby wybrać kierunek zwiedzania");
        System.out.println("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
        gadyczyssaki();
    }

    private static void gadyczyssaki() {

        System.out.println("\nstajesz przed wyborem do której cześci zoo  chcesz pójść.");
        System.out.println("wybierz 1 jeżeli chcesz iść do ssaków");
        System.out.println("wybierz 2 jeżeli chcesz iść do gadów");
        int a;
        System.out.print("wybieram: ");
        do {
            a = scanner.nextInt();
            if (a == 1) {
                System.out.println("witamy Cie u naszych ssaków");
                wyborSsaki();
                break;
            } else if (a == 2) {
                wyborGady();
                break;
            }
            System.out.println("ponów wybór: ");
        } while (true);
    }

    private static void wyborGady() {
    }

    private static void wyborSsaki() {
        Animal animal;
        System.out.println("\n1 - słoń \n2 - lew\n3 - niedzwiedz\n4 - wroc na poczatek");
        System.out.print("wybierz co chcesz zrobić: ");
        int a = scanner.nextInt();
        if(a>0 && a< 4){
            animal = ListaZwierzat.getSsak(a);
            System.out.printf("zapraszamy Cię do naszego "+animal.getClass().getSimpleName() + " " +animal.getImie());
            interakcja(animal);
        }
        else if (a==4){
            gadyczyssaki();
        }
        else{
            wyborSsaki();
        }

    }

    private static void interakcja(Animal animal) {

    }
}
