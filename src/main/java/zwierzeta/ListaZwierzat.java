package zwierzeta;

import java.util.ArrayList;
import java.util.List;

public class ListaZwierzat {
    private static List<Ssaki> listaSSakow = stworzListeSsakow();
    private static List<Gady> listagadow = stworzListeGadow();

    private static List<Gady> stworzListeGadow() {
        List<Gady> gady = new ArrayList<Gady>();
        Gekon gekon = new Gekon("alek", 2, 0.2, 2);
        Legwan legwan = new Legwan("drakus", 3, 2, 5);
        Kameleon kameleon = new Kameleon("olek", 4, 1, 3);
        gady.add(gekon);
        gady.add(legwan);
        gady.add(kameleon);
        return gady;
    }

    public static List<Ssaki> stworzListeSsakow() {
        List<Ssaki> ssaki = new ArrayList<Ssaki>();
        Slon slon = new Slon("mietek", 22, 150, 30);
        Lew lew = new Lew("Maniek", 10, 60, 60);
        Niedzwiedz niedzwiedz = new Niedzwiedz("Borys", 6, 90, 50);
        ssaki.add(slon);
        ssaki.add(lew);
        ssaki.add(niedzwiedz);
        return ssaki;
    }

    public static Ssaki getSsak(int index) {
        return listaSSakow.get(index - 1);
    }

    public static Gady getGad(int index) {
        return listagadow.get(index - 1);
    }

    public static void stworzListe() {
        stworzListeGadow();
        stworzListeSsakow();
    }
}
