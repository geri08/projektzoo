package zwierzeta;


public class Slon extends Ssaki implements Zabawa, Nakarm, Odpoczynek {
    private int zadowolenie = 10;
    private int zmeczenie = 10;
    private int glod = 10;


    public Slon(String imie, int wiek, double waga, double powierzchniawybieguM2) {
        super(imie, wiek, waga, powierzchniawybieguM2);
    }

    public void bawSie() {
        sprawdz(zmeczenie,glod);
        System.out.printf("Wziąłeś %s na spacer\n", imie);
        zadowolenie++;
        zmeczenie -= 2;
        glod -= 1;

    }

    public void poglaszcz() {
        sprawdz(zmeczenie,glod);
        System.out.printf("pogłaskałeś %s\n", imie);
        zmeczenie--;
        glod--;
        zadowolenie++;
    }

    public void dajSmakolyk() {
        sprawdz(zmeczenie,glod);
        System.out.printf("%s dostał swój przysmak\n", imie);
        zadowolenie++;
        glod++;
    }

    public void dajPosilek(double ilePokarmu) {
        sprawdz(zmeczenie,glod);
        System.out.printf("nakarmiłeś %s \n", imie);
        glod += 3;
        zmeczenie -= 2;
    }

    public void drzemka() {
        if (zmeczenie < 6) {
            System.out.printf("%s jest zmęczony, poszedł spać\n", imie);
            zmeczenie += 5;
        } else {
            System.out.printf("%s nie jest zmęczony, chce się bawić\\jeść\n", imie);
        }
    }


    @Override
    public String toString() {
        return "Slon{" +
                "imie='" + imie + '\'' +
                ", wiek=" + wiek +
                ", waga=" + waga +
                ", zadowolenie=" + zadowolenie +
                ", zmeczenie=" + zmeczenie +
                ", glod=" + glod +
                '}';
    }
}
