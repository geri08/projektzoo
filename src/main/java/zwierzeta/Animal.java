package zwierzeta;

public abstract class Animal implements Nakarm, Odpoczynek, Zabawa {
     String imie;
 int wiek;
 double waga;

    public String getImie() {
        return imie;
    }

    public int getWiek() {
        return wiek;
    }

    public double getWaga() {
        return waga;
    }


    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public void setWaga(double waga) {
        this.waga = waga;
    }

    public Animal(String imie, int wiek, double waga) {
        this.imie = imie;
        this.wiek = wiek;
        this.waga = waga;
    }

    public void sprawdz(int zmeczenie, int glod) {
        if (zmeczenie < 5) {
            System.out.printf("%s jest zmęczony, powinien odpoczac \n", imie);
        }
        if (glod < 5) {
            System.out.printf("%s jest głodny, powinien zjeść\n", imie);
        }
    }

    public void dajSmakolyk() {
        System.out.printf("nakarmiłeś %s \n", imie);
    }

    public void dajPosilek(double ilePokarmu) {
        System.out.printf("nakarmiłeś %s \n", imie);
    }

    public void bawSie() {
        System.out.printf("Wziąłeś %s na spacer\n", imie);

    }

    public void poglaszcz() {
        System.out.printf("pogłaskałeś %s\n", imie);
    }


    public void drzemka() {
        System.out.printf("%s poszedł spać \n",imie);
    }




}
