package zwierzeta;

public abstract class Ssaki extends Animal {
    private double powierzchniawybieguM2;

    public Ssaki(String imie, int wiek, double waga, double powierzchniawybieguM2) {
        super(imie, wiek, waga);
        this.powierzchniawybieguM2 = powierzchniawybieguM2;
    }
}