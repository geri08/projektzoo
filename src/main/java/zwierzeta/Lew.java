package zwierzeta;

public class Lew extends Ssaki  {
    private int zadowolenie = 12;
    protected int zmeczenie = 7;
    private int glod = 8;

    public Lew(String imie, int wiek, double waga, double powierzchniawybieguM2) {
        super(imie, wiek, waga, powierzchniawybieguM2);
    }

    @Override
    public void dajSmakolyk() {
        super.dajSmakolyk();
        glod++;
        zadowolenie++;
    }

    @Override
    public void dajPosilek(double ilePokarmu) {
        super.dajPosilek(ilePokarmu);
        glod +=3;
        zmeczenie-=2;
        super.setWaga(getWaga() + (0.7*ilePokarmu));
    }

    @Override
    public void bawSie() {
        super.bawSie();
        zadowolenie+=2;
        glod-=2;
        zmeczenie-=3;
        setWaga(getWaga() * 0.98);
    }

    @Override
    public void poglaszcz() {
        super.poglaszcz();
        zadowolenie++;
    }

    @Override
    public void drzemka() {
        if (zmeczenie<5){
            zmeczenie+=5;
        super.drzemka();}
        else {
            System.out.printf("%s nie jest zmęczony, woli się bawić lub coś zjeść\n",getImie());
        }
    }

    @Override
    public String toString() {
        return "Lew{" +
                "imie='" + getImie() + '\'' +
                ", wiek=" + getWiek() +
                ", waga=" + getWaga() +
                ", zadowolenie=" + zadowolenie +
                ", zmeczenie=" + zmeczenie +
                ", glod=" + glod +
                '}';
    }
}
