package zwierzeta;

public abstract class Gady extends Animal {
    double powierzchniaTerrariumM2;

    public Gady(String imie, int wiek, double waga, double powierzchniaTerrariumM2) {
        super(imie, wiek, waga);
        this.powierzchniaTerrariumM2 = powierzchniaTerrariumM2;
    }
}
